package billing;

import room.Room;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Billing system
 * @author Matthew Jennings
 * @author Simon Krahé
 */
public class Billing {
    private ArrayList<Room> roomsAssociated;
    private LocalDate checkIn;
    private int reservationNumber, reservationType;
    
    /**
     * Creates a billing object. Each billing object is associated to one reservation.
     * @param roomsAssociated All rooms that are part of this reservation
     * @param reservationType Type of reservation (S/AP)
     * @param checkIn Check-in date
     * @param reservationNumber The reservation number
     */
    public Billing(ArrayList<Room> roomsAssociated, int reservationType, LocalDate checkIn, int reservationNumber){
        this.roomsAssociated=roomsAssociated;
        this.checkIn=checkIn;
        this.reservationNumber=reservationNumber;
        this.reservationType=reservationType;
    }

    /**
     * Calculates the cost difference that occurs when a cancellation is issued
     * @param timeOfCancellation The time when the cancellation was issued
     * @return Difference in balance
     */
    public double calculateCancellationCost(LocalDateTime timeOfCancellation){
        if(this.reservationType==1){ //AP
            return 0;
        }
        if(this.reservationType==0 && this.checkIn.atTime(18, 0).minusHours(48).isAfter(timeOfCancellation)){ //S, more than 48h
            return -1*this.calculateReservationCost();
        }
        return 0; //within 48 hours, S
    }

    /**
     * Calculates the cost for the reservation
     * @return Cost for the reservation
     */
    public double calculateReservationCost(){
        double cost=0;
        for(int i=0; i<this.roomsAssociated.size(); i++){
            Room room=this.roomsAssociated.get(i);
            cost+=room.getRate(reservationNumber);
        }
        return (this.reservationType==0) ? cost : cost*0.95;
    }
}