package csvcreation;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import room.Room;
import reservation.Reservation;

/**
 * System for reading the C.S.V. file
 * @author Mahir Jha
 * @author Simon Krahé
 */
public class ReadCSVFile {
    /**
     * Private constructor to prevent instantiation
     */
    private ReadCSVFile(){
        throw new IllegalStateException("Utility class");
    }

    /**
     * Main method, which creates all of the rooms
     * @throws FileNotFoundException
    */
    public static void createRoomsFromFile() throws FileNotFoundException{
        java.io.File file = new java.io.File("l4Hotels.csv");
         System.out.println(file.getAbsolutePath());
        Scanner input = new Scanner(file);
        String lastHotelType="";
        input.useDelimiter(Pattern.compile("(\\n)|;"));    
        input.next();
        input.next();
        ArrayList<Room> rooms = new ArrayList<Room>();
        while(input.hasNext()){
            String line = input.next();
            Scanner inString = new Scanner(line);
            inString.useDelimiter(",");
            
            String hotelType = inString.next();
            String roomType;
            if(hotelType.matches("[0-9].*")){
                lastHotelType = hotelType;
                roomType = inString.next();
            }else{
                roomType = hotelType; 
                hotelType = lastHotelType;
            }
            //Changing "Deluxe Single" to "Single"
            String[] roomTypeSplit = roomType.split("\\s+");
            roomType = roomTypeSplit[roomTypeSplit.length-1];

            int noOfRooms = Integer.parseInt(inString.next());
            int occupancyMin = Integer.parseInt(inString.next());
            int occupancyMax = Integer.parseInt(inString.next());
            double mondayRate = Double.parseDouble(inString.next());
            double tuesdayRate = Double.parseDouble(inString.next());
            double wednesdayRate = Double.parseDouble(inString.next());
            double thursdayRate = Double.parseDouble(inString.next());
            double fridayRate = Double.parseDouble(inString.next());
            double saturdayRate = Double.parseDouble(inString.next());
            double sundayRate = Double.parseDouble(inString.next());

            for(int i=0;i<noOfRooms;i++){
                double[] arr = {mondayRate, tuesdayRate, wednesdayRate, thursdayRate, fridayRate, saturdayRate, sundayRate};
                rooms.add(new Room(roomType, occupancyMin, occupancyMax, arr, hotelType));
            }
            
            inString.close();
            
        }
        input.close();
        Reservation.setRooms(rooms);
    }
}