package printToCSV;

import room.Room;
import reservation.Reservation;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Prints current system information to CSV
 * @author Simon Krahé
 */
public class PrintToCSV {

    /**
     * Private constructor to prevent instantiation
     */
    private PrintToCSV(){
        throw new IllegalStateException("Utility class");
    }

    /**
     * Prints all reservations to file
     * @throws FileNotFoundException
     */
    public static void printAllReservations() throws FileNotFoundException{
        
        java.io.File csvoutput = new java.io.File("outputReservations.csv");
        PrintWriter pw = new java.io.PrintWriter(csvoutput);

        ArrayList<Reservation> reservationList = Reservation.getReservationList();
        for(int i=0; i<reservationList.size(); i++){
            pw.println(reservationList.get(i).toCSV());
        }
        pw.close();
    }

    /**
     * Prints all rooms to CSV
     * @throws FileNotFoundException
     */
    public static void printAllRooms() throws FileNotFoundException{
        java.io.File csvoutput = new java.io.File("outputRooms.csv");
        PrintWriter pw = new java.io.PrintWriter(csvoutput);

        ArrayList<Room> roomList;
        roomList=Reservation.getRooms();
        pw.println("RoomType,MinimumOccupancy,MaximumOccupancy,HotelName,RoomNumber,MondayRate/TuesdayRate/WednesdayRate/ThursdayRate/FridayRate/SaturdayRate/SundayRate,ReservationID:Checkin-Checkout:Occupancy/OtherReservationID:Checkin-Checkout:Occupancy");
        for(int i=0; i<roomList.size(); i++){
            pw.println(roomList.get(i).toCSV());
        }
        pw.close();
    }
}