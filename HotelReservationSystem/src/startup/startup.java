package startup;

import UI.GUI;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import time.Time;
import UI.UI;
import csvcreation.ReadCSVFile;
import printToCSV.PrintToCSV;

/**
 * Startup Class for System Initialization
 * @author Simon Krahé
 */
public class startup {
    /**
     * System startup method
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String args[]) throws FileNotFoundException{
        ReadCSVFile.createRoomsFromFile();
        //we are setting the time to January 1. This date can be used to test if cancellation is working as intended
        LocalDateTime currentTime = LocalDateTime.of(2021, 1, 1, 14, 30);
        Time.setTime(currentTime);
        UI ui = new UI();
        ui.run();
        GUI.main(args);
        PrintToCSV.printAllReservations();
        PrintToCSV.printAllRooms();
    }
}