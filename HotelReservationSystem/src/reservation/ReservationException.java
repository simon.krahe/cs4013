package reservation;

/**
 * Exception that is thrown by the reservation System
 * @author Simon Krahé
 */
public class ReservationException extends Exception { 
    public ReservationException(String errorMessage) {
        super(errorMessage);
    }
}