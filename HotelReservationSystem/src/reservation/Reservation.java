package reservation;

import room.Room;
import time.Time;
import billing.Billing;
import java.util.ArrayList;
import java.time.LocalDate;
import room.RoomWithoutCost;

/**
 * Reservation system
 * @author Simon Krahé
 */
public class Reservation {
    private static int reservationCounter;
    private static ArrayList<Room> rooms;
    private static ArrayList<Reservation> reservationList = new ArrayList <Reservation>();
    private int reservationNumber, reservationType;
    private LocalDate checkIn, checkOut;
    private ArrayList<Room> roomsAssociated = new ArrayList<Room>();
    private boolean successful = false; 
    private Billing billing;
    private double balance;

    /**
     * Creates a reservation
     * @param reservationType Type of reservation (S/AP)
     * @param checkIn Check-in date
     * @param checkOut Check-out date
     * @param roomsWanted An arraylist of rooms that match the reservation request (these are created just for this request and none of the hotel's existing rooms)
     */
    public Reservation(int reservationType, LocalDate checkIn, LocalDate checkOut, ArrayList<RoomWithoutCost> roomsWanted) throws ReservationException{
        this.reservationNumber=Reservation.reservationCounter++;
        //The day of the checkout is not counted on a reservation
        if (checkOut.isAfter(checkIn))
            checkOut=checkOut.minusDays(1);
        if((checkIn.isBefore(checkOut) || checkIn.isEqual(checkOut)) && findRooms(checkIn, checkOut, roomsWanted)){
            this.reservationType=reservationType;
            this.successful=true;
            this.checkIn=checkIn;
            this.checkOut=checkOut;

            this.billing = new Billing(this.roomsAssociated, this.reservationType, this.checkIn, this.reservationNumber);
            this.balance=billing.calculateReservationCost();
        }
        else{
            Reservation.reservationCounter--;
            this.clearRoomsAssociated();
            throw new ReservationException("The requested rooms are not available at the requested time. The reservation request can not be fulfilled. No reservation has been made");
            }
        }

    /**
     * Getter method to find out if the reservation was fulfilled successfully
     * @return Status of whether the reservation was successfully
     */
    public boolean getSuccessfulReservation(){
        return this.successful;
    }

    /**
     * Setter method for all of the available rooms. To be used at program startup.
     * @param rooms All available rooms
     */
    public static void setRooms(ArrayList<Room> rooms){
        Reservation.rooms=rooms;
    }

    /**
     * Tries to accommodate room requests. Makes reservations along the way, even if the total request cannot be fulfilled
     * @param checkIn Check-in date
     * @param checkOut Check-out date
     * @param roomsWanted An arraylist of rooms that match the reservation request (these are created just for this request and none of the hotel's existing rooms)
     * @return whether the request was fulfilled.
     */
    private boolean findRooms(LocalDate checkIn, LocalDate checkOut, ArrayList<RoomWithoutCost> roomsWanted){
        for(int i=0; i<roomsWanted.size(); i++){
            RoomWithoutCost spec=roomsWanted.get(i);
            spec.reserve(checkIn, checkOut, 42, 1); //42 is a dummy reservation ID, 1 is a dummy occupancy
            Boolean filled=false;
            for(int j=0; j<rooms.size() && filled==false; j++){
                Room candidate=rooms.get(j);
                if(candidate.canFill(spec)){
                    candidate.reserve(spec.getCheckin(), spec.getCheckout(), this.reservationNumber, spec.getOccupancyMax()); //All the information on room 
                    this.roomsAssociated.add(candidate);
                    filled=true;
                }
            }
        }
        Boolean fulfilled = (roomsWanted.size()==this.roomsAssociated.size()) ? true : false;
        if(fulfilled){
            Reservation.reservationList.add(this);
            return true;
        }
        return false;
    }

    /**
     * Checks if this reservation matches a given reservation number
     * @param number The reservation number to be checked
     * @return Whether the reservation number matches
     */
    private Boolean isNumber(int number){
        return (this.reservationNumber==number) ? true : false;
    }

    /**
     * Finds a reservation matching the reservation number
     * @param reservationNumber The reservation number
     * @return The reservation matching the number (if there is one)
     */
    private static Reservation findReservation(int reservationNumber) throws IndexOutOfBoundsException{
        int number=-1;
        for (int i=0; i<Reservation.reservationList.size(); i++){
            if (Reservation.reservationList.get(i).isNumber(reservationNumber)){
                number=i;
            }
        }
        Reservation reservation = Reservation.reservationList.get(number);
        return reservation;
    }

    /**
     * Returns the balance for a reservation with a specified ID 
     * @param reservationNumber The reservation ID
     * @return The total cost
     */
    public static double getBalance(int reservationNumber){
        try {
            Reservation reservation = Reservation.findReservation(reservationNumber);
            return reservation.balance;
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Reservation not found, dummy balance of -1 returned");
            return -1;
        }
    }

    /**
     * Gets the reservation number
     * @return The number for this reservation
     */
    public int getReservationNumber(){
        return this.reservationNumber;
    }
    

    /**
     * Cancels the entire reservation. This is the representation of "Cancel a reservation" and calls other internal methods to do this.
     * @param reservationNumber the identifier for this reservation
     */
    public static void cancelReservation(int reservationNumber) throws ReservationException{
        try {
            Reservation toCancel = Reservation.findReservation(reservationNumber);
            double cancellationCost=toCancel.billing.calculateCancellationCost(Time.getTime());
            toCancel.clearRoomsAssociated();
            toCancel.balance+=cancellationCost;
        } catch (IndexOutOfBoundsException e) {
            throw new ReservationException("Reservation could not be found, not cancelled");
        }
    }

    /**
     * Clears reservations on rooms associated with this reservation
     */
    private void clearRoomsAssociated(){
        for(int i=0; i<this.roomsAssociated.size(); i++){
            this.roomsAssociated.get(i).unreserve(this.reservationNumber);
            this.roomsAssociated.remove(i);
        }
    }
    
    /**
     * Dictionary method on reservation types for use by the whole system
     * @param type reservation type stored as an integer
     * @return reservation type stored as a string
     */
    public static String reservationTypeToString(int type){
        return (type==0) ? "S" : "AP";
    }

    /**
     * Dictionary method on reservation types for use by the whole system
     * @param type reservation type stored as an string
     * @return reservation type stored as a integer
     */
    public static int reservationTypeToInt(String type){
        return (type.equals("S")) ? 0 : 1;
    }

    /**
     * Returns a string to print the reservation information in CSV format
     * @return CSV-string
     */
    public String toCSV(){
        String result="";
        result += this.reservationNumber + "," + this.reservationType + "," + this.checkIn.toString() + "," + this.checkOut.toString() + ",";
        int i;
        for(i=0; i<this.roomsAssociated.size(); i++){
            result+=this.roomsAssociated.get(i).getRoomNumber();
            if (i+1<this.roomsAssociated.size())
                result+="/";
        }
        result+=",";
        result+=this.balance;
        return result;
    }

    /**
     * Gives an array of all reservations. Note: please keep operations on this array contained within the Reservation class, this method is only for other information like the amount of reservations
     * @return ArrayList of Reservation objects
     */
    public static ArrayList<Reservation> getReservationList(){
        return Reservation.reservationList;
    }

    /**
     * Gives an array of all rooms. Note: please keep operations on this array contained within the Room or Reservation class, this method is only for other information like the amount of rooms
     * @return ArrayList of Room objects
     */
    public static ArrayList<Room> getRooms(){
        return Reservation.rooms;
    }
}