package analysis;
import java.time.LocalDate;
import java.util.ArrayList;
import room.Room;

import reservation.Reservation;

/**
 * Class to provide analysis data
 * @author Simon Krahé
 */
public class Analysis {
    LocalDate start;
    LocalDate end;
    String hotelAndRoomTypeFigures;
    String billingFigures;


    /**
     * Creates a new analysis object for a certain period of time
     * @param start Start period of analysis
     * @param end End period of analysis
     */
    public Analysis(LocalDate start, LocalDate end){
        this.end=end;
        this.start=start;
        ArrayList<Room> roomList = Reservation.getRooms();

        ArrayList<Integer> reservationNumbers = new ArrayList<Integer>();
        ArrayList<String> hotelTypesByReservationNumber = new ArrayList<String>();

        ArrayList<String> hotelTypes = Room.getAllHotelTypes();
        ArrayList<String> roomTypes = Room.getAllRoomTypes();
        //two dimensional array, stores days reserved based on a combination of hotel and room type
        int[][] daysReserved = new int[hotelTypes.size()][roomTypes.size()];


        for (int i=0; i<roomList.size(); i++){
            for(LocalDate now = start; now.isBefore(end); now=now.plusDays(1)){
                Room ourRoom=roomList.get(i);
                //for each room for each day, add a day reserved at the right position in daysReserved if occupied on that day
                if (ourRoom.isOccupiedOn(now)){
                    daysReserved[hotelTypes.indexOf(ourRoom.getHotel())][roomTypes.indexOf(ourRoom.getRoomType())]++;
                    //store reservation number for later reuse
                    reservationNumbers.add(ourRoom.getReservationIDOnDay(now));
                    hotelTypesByReservationNumber.add(ourRoom.getHotel());
                } }
        }
        String result="";
        for (int i=0; i<hotelTypes.size(); i++){
            for (int j=0; j<roomTypes.size(); j++){
                //print the total array to a result string
                result+="Hotel " + hotelTypes.get(i) + ", Room Type " + roomTypes.get(j) + " is occupied for a total of " + String.valueOf(daysReserved[i][j]) + " days.\n";
            }
        }
        this.hotelAndRoomTypeFigures=result;


        ArrayList<Integer> reservationNumbersWithoutDuplicates = new ArrayList<Integer>();
        ArrayList<String> hotelTypesByReservationNumberWithoutDuplicates = new ArrayList<String>();
        //above, we added to these arraylists for every day reserved, we are 'shrinking' to one entry per reservation. this is needed because there can be multiple reservations on one room.
        for (int i=0; i<reservationNumbers.size(); i++){
            if (!reservationNumbersWithoutDuplicates.contains(reservationNumbers.get(i))){
                reservationNumbersWithoutDuplicates.add(reservationNumbers.get(i));
                hotelTypesByReservationNumberWithoutDuplicates.add(hotelTypesByReservationNumber.get(i));
            }
        }

        double total[] = new double[hotelTypes.size()];
        //we are cycling through the reservations and adding their balance to the associated hoteltype in this balance array
        for (int i=0; i<reservationNumbersWithoutDuplicates.size(); i++){
            total[hotelTypes.indexOf(hotelTypesByReservationNumberWithoutDuplicates.get(i))]
                +=Reservation.getBalance(reservationNumbersWithoutDuplicates.get(i));
        }
        result="";
        for (int i=0; i<hotelTypes.size(); i++){
            //adding the balance based on hoteltype to the result string
            result+="Hotel " + hotelTypes.get(i) + " is responsible for a total balance of " + String.valueOf(total[i]) + "\n";
        }
        this.billingFigures=result;
    }

    /**
     * Hotel and Room Type Analysis Figures
     * @return String of Figures
     */
    public String getHotelAndRoomTypeFigures(){
        return this.hotelAndRoomTypeFigures;
    }

    /**
     * Billing Analysis Figures
     * @return String of Figures
     */
    public String getBillingFigures(){
        return this.billingFigures;
    }
}