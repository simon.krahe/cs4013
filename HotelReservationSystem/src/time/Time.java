package time;

import java.time.LocalDateTime;

/**
 * Current time storage system
 * @author Mahir Jha
 */
public class Time{
    private static LocalDateTime currentTime;
    /**
     * Private constructor to prevent instantiation
     */
    private Time(){
        throw new IllegalStateException("Utility class");
    }
    
     /**
     * Getter method to get current time
     * @return Current time
     */
    public static LocalDateTime getTime(){
        return Time.currentTime;
    }
    
     /**
     * Sets time
     * @param time Desired time
     */
    public static void setTime(LocalDateTime time){
        Time.currentTime = time;
    }
}