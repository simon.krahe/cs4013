package UI;

import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

import analysis.Analysis;
import reservation.Reservation;
import reservation.ReservationException;
import room.RoomWithoutCost;
import time.Time;
/**User Interface for Hotel Reservation System
 * @author Matthew Jennings
 * @author Simon Krahé
*/
public class UI{
    private Scanner inputScan;
    private int reservationType;
    private String roomType, hotelType;
    private int occ;
    private ArrayList<RoomWithoutCost> roomsWanted = new ArrayList<RoomWithoutCost>();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    boolean admin = false;
    /**
     * User Interface, contains creation of scanner and a password check
     */
    public UI(){
        this.inputScan = new Scanner(System.in);
        System.out.println("Please input the admin password to access admin methods. Hint: It is 122+1");
        String password = inputScan.nextLine();
        if (password.equals("123"))
            this.admin=true;
    }

    /**
     * Runs the creation of the UI and books rooms, holds the variables necessary to create room objects,
     * cancels bookings, changes time and analyse room occupancies between two dates
     */
    public void run()
    {
        boolean q=false;
        while(!q){
            System.out.println("Select function: (B)ook a Room; (C)ancel a Booking; set the (T)ime; get (A)nalysis date; (Q)uit.");
            String function = inputScan.nextLine();

            if(function.equals("B")){
                System.out.println("Enter the date you wish to book your room from: (YYYY-MM-DD)");
                String checkIn = inputScan.nextLine();
                LocalDate checkInDate = LocalDateTime.parse(checkIn + " 10:00", formatter).toLocalDate();
                System.out.println("Enter the date on which you plan to check out: (YYYY-MM-DD)");
                String checkOut = inputScan.nextLine();
                LocalDate checkOutDate = LocalDateTime.parse(checkOut + " 10:00", formatter).toLocalDate();


                String command = "dummy";
                while(!(command.equals("S") || command.equals("AP"))){
                    System.out.println("Enter the type of reservation you wish to make: S for Standard, AP for Advanced Purchase: ");
                    command = inputScan.nextLine().toUpperCase();
                    if(command.equals("S")){
                        reservationType = 0;
                    }else if(command.equals("AP")){
                        reservationType = 1;
                    }
                }

                boolean cont = true;
                hotelType="dummy";
                while(!(hotelType.equals("5-star") || hotelType.equals("4-star") || hotelType.equals("3-star"))){
                    System.out.println("Please input the hotel type\n" + 
                    "The room types for 5 star hotels are: Single, Twin, Double, Family\n" + 
                    "The room types for 4 star hotels are: Single, Twin, Double\n" + 
                    "The room types for 3 star hotels are: Single, Twin, Double\n" + 
                    "Enter the room type you wish to book: D for 5 star, E for 4 star, C for 3 star: ");
                    String hotelTypeInput = inputScan.nextLine().toUpperCase();

                    if(hotelTypeInput.equals("D")){
                        hotelType="5-star";
                    }else if(hotelTypeInput.equals("E")){
                        hotelType="4-star";
                    }else if(hotelTypeInput.equals("C")){
                        hotelType="3-star";
                    }else{
                        System.out.println("Hotel type not currently supported. Please try again.");
                    }
                }

                cont = true;
                while(cont){

                    //room type selection
                    roomType="dummy";
                    while(!(roomType.equals("Single") || roomType.equals("Double") || roomType.equals("Twin") || roomType.equals("Family"))){
                        System.out.println("Please input the room type. Valid choices are \"Single\", \"Twin\", \"Double\", \"Family\"");
                        roomType = inputScan.nextLine();
                    }

                    occ = 0;
                    while(occ<1 || occ >3){
                        System.out.println("Choose your occupancy. Single room allow for one, Double and Twin for two, Family for three.");
                        occ = inputScan.nextInt();
                        inputScan.nextLine();
                    }

                    roomsWanted.add(new RoomWithoutCost(roomType, 1, occ, hotelType));

                    System.out.println("Do you want to add an additional room? (Y)/anything");
                    String continueInput = inputScan.nextLine();
                    cont=false;
                    if(continueInput.toUpperCase().equals("Y")){
                        cont = true;
                        roomType="dummy";
                        occ=0;
                    }
                }

                try {
                    Reservation reservation = new Reservation(reservationType, checkInDate, checkOutDate, roomsWanted);
                    roomsWanted.clear();
                    int reservationNumber = reservation.getReservationNumber();
                    System.out.println("Your booking ID is: " + reservationNumber);
                    double balance = Reservation.getBalance(reservationNumber);
                    System.out.println("Your outstanding balance is: " + balance);
                } catch (ReservationException e) {
                    System.out.println("Reservation could not be fulfilled");
                }
            }

            else if(function.equals("C")){
                System.out.println("Please enter the reservation ID to cancel");
                int bookingId = inputScan.nextInt();
                inputScan.nextLine();
                try {
                    Reservation.cancelReservation(bookingId);
                    System.out.println("Reservation cancelled. Remaining balance: " + Reservation.getBalance(bookingId));
                } catch (ReservationException e) {
                    System.out.println("Reservation could not be cancelled.");
                }
            }

            else if (function.equals("T")){
                if(this.admin){
                    System.out.println("Please input the time (YYYY-MM-DD HH-MM)");
                    String timeInput = inputScan.nextLine();
                    LocalDateTime time = LocalDateTime.parse(timeInput, formatter);
                    Time.setTime(time);
                }
                else{
                    System.out.println("This method is not available to you because you are not an admin.");
                }
            }

            else if(function.equals("A")){
                if(this.admin){
                    System.out.println("Please input the start and the end time on each line (YYYY-MM-DD)");
                    String timeInput1 = inputScan.nextLine();
                    String timeInput2 = inputScan.nextLine();
                    LocalDate time1 = LocalDateTime.parse(timeInput1 + " 03:00", formatter).toLocalDate();
                    LocalDate time2 = LocalDateTime.parse(timeInput2 + " 03:00", formatter).toLocalDate();
                    Analysis analysis = new Analysis(time1, time2);
                    System.out.println("Billing figures:\n" + analysis.getBillingFigures());
                    System.out.println("Hotel and room type figures:\n" + analysis.getHotelAndRoomTypeFigures());
                }
                else{
                    System.out.println("This method is not available to you because you are not an admin.");
                }
            }

            else if(function.equals("Q")){
                q=true;
                this.inputScan.close();
            }
        }
    }
}