package UI;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import room.RoomWithoutCost;
import time.Time;
import reservation.Reservation;
import reservation.ReservationException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import analysis.Analysis;
/** Graphical User Interface for Hotel Reservation System
 * @author Matthew Jennings
 * @author Simon Krahé
 */
public class GUI extends Application{
    //values for making the reservation
    String roomTypeSelection, hotelName;
    int occupancy, starsType, reservationType;
    LocalDate valueIn, valueOut;
    boolean admin;
    ArrayList<RoomWithoutCost> roomsToRequest = new ArrayList<RoomWithoutCost>();
    Reservation reservation;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    ArrayList<String> hotelNamesUsed = new ArrayList<String>();

    /**
     * Creates RoomWithoutCost object, appends it to the roomsToRequest list
     * @param roomTypeSelection Room Type Selected
     * @param occupancy Occupancy of Hotel
     * @param hotelName Name of Hotel
     */
    private void appendRoomToRequest(String roomTypeSelection, int occupancy, String hotelName){
      roomsToRequest.add(new RoomWithoutCost(roomTypeSelection, 1, occupancy, hotelName));
      if(!hotelNamesUsed.contains(hotelName)){
        hotelNamesUsed.add(hotelName);
      }
    }
    /**
     * Checks if user tries to book a room in more than 1 hotel
     * @return returns a true value if user tries to book in more than 1 hotel, false otherwise.
     */
    private boolean checkIfMultipleHotelsUsed(){
      if (hotelNamesUsed.size()>1){
        hotelNamesUsed.clear();
        return true;
      }
      hotelNamesUsed.clear();
      return false;
    }
    @Override
    /**
     * Runs the creation of the GUI and adds all it's scenes, panes, buttons, labels and text fields.
     * Also books rooms, holds the variables necessary to create room objects,
     * cancels bookings, changes time and analyse room occupancies between two dates
     * @param mainMenu The base stage for the GUI, it's scenes and panes
     */
    public void start(Stage mainMenu){
      GridPane passwordGridPane = new GridPane();
      Scene passwordCheck = new Scene(passwordGridPane);
      GridPane mainMenuGridPane = new GridPane();
      Scene mainMenuScene = new Scene(mainMenuGridPane);
      GridPane reservationDateGridPane = new GridPane();
      Scene dateForReservationScene = new Scene(reservationDateGridPane);
      GridPane reservationDetailsGridPane = new GridPane();
      Scene reservationDetailsScene = new Scene(reservationDetailsGridPane);
      GridPane exitGridPane = new GridPane();
      Scene exitBooking = new Scene(exitGridPane);
      GridPane failedGridPane = new GridPane();
      Scene reservationFailed = new Scene(failedGridPane);
      GridPane cancelGridPane = new GridPane();
      GridPane adminFailed = new GridPane();
      Scene adminCheckFailed = new Scene(adminFailed);
      GridPane afterCancellationGridPane = new GridPane();
      Scene afterCancellation = new Scene(afterCancellationGridPane);

      passwordGridPane.setPadding(new Insets(100));
      passwordGridPane.setHgap(10);
      passwordGridPane.setVgap(10);
      passwordGridPane.setPrefWidth(500);
      Button backToMenuButton = new Button("Return to main menu");
      backToMenuButton.setOnAction(e -> {
        mainMenu.setScene(mainMenuScene);
      });
      backToMenuButton.setPrefWidth(passwordGridPane.getPrefWidth());

      TextField adminPass = new TextField("Please enter your admin password to access admin features.");
      adminPass.setPrefWidth(passwordGridPane.getPrefWidth());
      passwordGridPane.add(adminPass, 0, 0);

      Button backToMenuFromPasswordButton = new Button("Submit password");
      backToMenuFromPasswordButton.setOnAction(e-> {
        if(adminPass.getText().equals("123")){
          admin = true;
        }else{
          admin = false;
        }
        mainMenu.setScene(mainMenuScene);
      });
      passwordGridPane.add(backToMenuFromPasswordButton, 0, 1);


      mainMenu.setTitle("Hotel Reservation System");    
      mainMenu.setScene(passwordCheck);
      mainMenu.show();
      Label checkFail = new Label("You are not authorized to use this function.");
      adminFailed.add(checkFail, 0, 0);
      Button backToMenuFromAdminFailButton = new Button("Return to main menu");
      backToMenuFromAdminFailButton.setOnAction(e -> {
        mainMenu.setScene(mainMenuScene);
      });
      adminFailed.add(backToMenuFromAdminFailButton, 0, 1);

      mainMenuGridPane.setPadding(new Insets(100));
      mainMenuGridPane.setHgap(10);
      mainMenuGridPane.setVgap(10);
      mainMenuGridPane.setPrefWidth(500);

      ArrayList<Button> buttonList = new ArrayList<Button>();
      buttonList.add(new Button("Book a Room"));
      buttonList.add(new Button("Cancel a Booking"));
      buttonList.add(new Button("Set time"));
      buttonList.add(new Button("Check analysis"));
      buttonList.add(new Button("Quit"));
    
      mainMenuGridPane.add(buttonList.get(0), 0, 0);
      mainMenuGridPane.add(buttonList.get(1), 0, 1);
      mainMenuGridPane.add(buttonList.get(2), 0, 2);
      mainMenuGridPane.add(buttonList.get(3), 0, 3);
      mainMenuGridPane.add(buttonList.get(4), 0, 4);
      
      buttonList.get(0).setMinWidth(mainMenuGridPane.getPrefWidth());
      buttonList.get(1).setMinWidth(mainMenuGridPane.getPrefWidth());
      buttonList.get(2).setMinWidth(mainMenuGridPane.getPrefWidth());
      buttonList.get(3).setMinWidth(mainMenuGridPane.getPrefWidth());
      buttonList.get(4).setMinWidth(mainMenuGridPane.getPrefWidth());

      reservationDateGridPane.setPadding(new Insets(100));
      reservationDateGridPane.setHgap(10);
      reservationDateGridPane.setVgap(10);

      Label dateIn = new Label("Enter the date you wish to book in: ");
      reservationDateGridPane.add(dateIn, 0, 0);
      DatePicker datePicker1 = new DatePicker();
      reservationDateGridPane.add(datePicker1, 0, 1);
      Label dateOut = new Label("Enter the date you wish to check out: ");
      reservationDateGridPane.add(dateOut, 1, 0);
      DatePicker datePicker2 = new DatePicker();
      reservationDateGridPane.add(datePicker2, 1, 1);

      buttonList.add(new Button("Next"));
      reservationDateGridPane.add(buttonList.get(5), 1, 3);

      reservationDateGridPane.setPadding(new Insets(100));
      reservationDateGridPane.setHgap(20);
      reservationDateGridPane.setVgap(20);

      Label bookingType = new Label("Choose the booking type:");
      reservationDetailsGridPane.add(bookingType, 0, 0);
      reservationDetailsGridPane.setPadding(new Insets(100));
      reservationDetailsGridPane.setHgap(10);
      reservationDetailsGridPane.setVgap(10);

      final ToggleGroup group1 = new ToggleGroup();
      RadioButton rb1 = new RadioButton();
      rb1.setText("Standard");
      rb1.setToggleGroup(group1);
      rb1.setOnAction(e->{
        reservationType=0;
      });
      
      RadioButton rb2 = new RadioButton();
      rb2.setText("Advanced Purchase");
      rb2.setToggleGroup(group1);
      rb2.setOnAction(e->{
        reservationType=1;
      });

      reservationDetailsGridPane.add(rb1, 0, 1);
      reservationDetailsGridPane.add(rb2, 0, 2);

      Label stars = new Label("Choose the hotel type:");
      reservationDetailsGridPane.add(stars, 1, 0);
      final ToggleGroup group2 = new ToggleGroup();
      RadioButton rb3 = new RadioButton();
      rb3.setText("5 Stars");
      rb3.setToggleGroup(group2);
      rb3.setOnAction(e->{
        starsType=5;
      });

      RadioButton rb4 = new RadioButton();
      rb4.setText("4 Stars");
      rb4.setToggleGroup(group2);
      rb4.setOnAction(e->{
        starsType=4;
      });

      RadioButton rb5 = new RadioButton();
      rb5.setText("3 Stars");
      rb5.setToggleGroup(group2);
      rb5.setOnAction(e->{
        starsType=3;
      });

      reservationDetailsGridPane.add(rb3, 1, 1);
      reservationDetailsGridPane.add(rb4, 1, 2);
      reservationDetailsGridPane.add(rb5, 1, 3);

      Label roomType = new Label("Choose the room type:");
      reservationDetailsGridPane.add(roomType, 2, 0);

      final ToggleGroup group3 = new ToggleGroup();

      RadioButton rb6 = new RadioButton();
      rb6.setText("Family");
      rb6.setToggleGroup(group3);
      rb6.setOnAction(e -> {
        roomTypeSelection = "Family";
      });

      RadioButton rb7 = new RadioButton();
      rb7.setText("Double");
      rb7.setToggleGroup(group3);
      rb7.setOnAction(e -> {
        roomTypeSelection="Double";
      });

      RadioButton rb8 = new RadioButton();
      rb8.setText("Twin");
      rb8.setToggleGroup(group3);
      rb8.setOnAction(e -> {
        roomTypeSelection="Twin";
      });

      RadioButton rb9 = new RadioButton();
      rb9.setText("Single");
      rb9.setToggleGroup(group3);
      rb9.setOnAction(e -> {
        roomTypeSelection="Single";
      });

      reservationDetailsGridPane.add(rb6, 2, 1);
      reservationDetailsGridPane.add(rb7, 2, 2);
      reservationDetailsGridPane.add(rb8, 2, 3);
      reservationDetailsGridPane.add(rb9, 2, 4);

      Label occupancyLabel = new Label("Choose your occupancy:");
      reservationDetailsGridPane.add(occupancyLabel, 4, 0);
      final ToggleGroup group4 = new ToggleGroup();
      RadioButton rb10 = new RadioButton();
      rb10.setText("1");
      rb10.setToggleGroup(group4);
      rb10.setOnAction(e -> {
        occupancy = 1;
      });

      RadioButton rb11 = new RadioButton();
      rb11.setText("2");
      rb11.setToggleGroup(group4);
      rb11.setOnAction(e -> {
        occupancy = 2;
      });

      RadioButton rb12 = new RadioButton();
      rb12.setText("3");
      rb12.setToggleGroup(group4);
      rb12.setOnAction(e -> {
        occupancy = 3;
      });

      reservationDetailsGridPane.add(rb10, 4, 1);
      reservationDetailsGridPane.add(rb11, 4, 2);
      reservationDetailsGridPane.add(rb12, 4, 3);
      failedGridPane.add(new Label("Reservation failed"), 0,0);
      Button failureButton = new Button("Return to menu");
      failureButton.setOnAction(e -> {
        mainMenu.setScene(mainMenuScene);
      });
      failedGridPane.setPadding(new Insets(100));
      failedGridPane.setHgap(10);
      failedGridPane.setVgap(10);
      failedGridPane.add(failureButton, 0, 1);

      Button nextButton = new Button("Next");
      reservationDetailsGridPane.add(nextButton, 3, 5);
      Button bookAnotherRoomButton = new Button("Book another Room (Buttons will remain set)");

      reservationDetailsGridPane.add(bookAnotherRoomButton, 1, 5);

      reservationDetailsGridPane.add(backToMenuButton, 5, 5);

      Label reserveSuccess = new Label("Your booking has been successfully completed\n\n\n");
      exitGridPane.add(reserveSuccess, 0, 0);
      Button successfulReservation = new Button("Return to menu");
      successfulReservation.setOnAction(e->{
        mainMenu.setScene(mainMenuScene);
      });
      exitGridPane.setPadding(new Insets(100));
      exitGridPane.setHgap(10);
      exitGridPane.setVgap(10);
    
      exitGridPane.add(successfulReservation, 0, 2);

      buttonList.get(0).setOnAction(e -> {
        mainMenu.setScene(dateForReservationScene);
      });

      buttonList.get(5).setOnAction(actionEvent -> {
        valueIn = datePicker1.getValue();
        valueOut = datePicker2.getValue();
        mainMenu.setScene(reservationDetailsScene);
      });

      //"next" button in booking screen
      nextButton.setOnAction(e -> {
        appendRoomToRequest(roomTypeSelection, occupancy, String.valueOf(starsType)+"-star");
        try {
          if(checkIfMultipleHotelsUsed())
            throw new ReservationException("Multiple Hotels used");
          reservation=new Reservation(reservationType, valueIn, valueOut, roomsToRequest);
          mainMenu.setScene(exitBooking);
          //todo resize this window
          reserveSuccess.setText("Your booking has been successfully completed\nYour booking ID is " + String.valueOf(reservation.getReservationNumber()) + "\nThe balance is " + Reservation.getBalance(reservation.getReservationNumber()));
        } catch (ReservationException failedReservationException) {
          mainMenu.setScene(reservationFailed);
        }
        finally{
          roomsToRequest.clear();
        }
      });

      bookAnotherRoomButton.setOnAction(actionEvent -> {
        if(((roomTypeSelection.equals("Double") || roomTypeSelection.equals("Twin")) && occupancy >= 3) || (roomTypeSelection.equals("Single") && occupancy >= 2)) {
          mainMenu.setScene(reservationFailed);
        }
        else{
          appendRoomToRequest(roomTypeSelection, occupancy, String.valueOf(starsType)+"-star");
          //todo clear radio buttons here
          mainMenu.setScene(reservationDetailsScene);
        }
      });

      Label cancelMessage = new Label("");
      Button backToMenuFromAfterCancellationButton = new Button("Return to main menu");
      backToMenuFromAfterCancellationButton.setOnAction(e->{
        mainMenu.setScene(mainMenuScene);
      });

      exitGridPane.setPadding(new Insets(100));
      exitGridPane.setHgap(10);
      exitGridPane.setVgap(10);
      afterCancellationGridPane.add(cancelMessage, 0, 0);
      afterCancellationGridPane.add(backToMenuFromAfterCancellationButton, 0, 1);
      TextField cancelTextField = new TextField("Please input you reservation ID to cancel");
      afterCancellationGridPane.add(cancelTextField, 0, 0);
      cancelGridPane.add(cancelTextField, 0, 0);
      Button cancelButton = new Button("Click to cancel");
      cancelGridPane.add(cancelButton, 0, 1);
      cancelButton.setOnAction(e -> {
        try {
          Reservation.cancelReservation(Integer.valueOf(cancelTextField.getText()));
          cancelMessage.setText("Your reservation has been cancelled\nThe remaining balance is: " + Reservation.getBalance(Integer.valueOf(cancelTextField.getText())));
        } catch (ReservationException cancelFailedException) {
          cancelMessage.setText("Your reservation could not be cancelled.");
        }
        finally{
          mainMenu.setScene(afterCancellation);
        }
      });

      Scene cancelReservationScene = new Scene(cancelGridPane);
      //cancel a booking button
      buttonList.get(1).setOnAction(e -> {
        mainMenu.setScene(cancelReservationScene);
      });

      //set time
      GridPane timeGridPane = new GridPane();
      timeGridPane.setPadding(new Insets(100));
      timeGridPane.setHgap(10);
      timeGridPane.setVgap(10);
      TextField enterTimeTextField = new TextField("Enter time in YYYY-MM-DD HH:MM format");
      Button timeBackToMenuButton = new Button("Submit and back to menu");
      timeGridPane.add(enterTimeTextField, 0, 0);
      timeGridPane.add(timeBackToMenuButton, 0, 1);
      timeBackToMenuButton.setOnAction(e->{
        Time.setTime(LocalDateTime.parse(enterTimeTextField.getText(), formatter));
        mainMenu.setScene(mainMenuScene);
      });

      Scene timeCreation = new Scene(timeGridPane);
      buttonList.get(2).setOnAction(e->{
        if(admin == false){
          mainMenu.setScene(adminCheckFailed);
        }else{
        mainMenu.setScene(timeCreation);
        }
      });

      //analysis
      GridPane analysisGridPane = new GridPane();
      analysisGridPane.setPadding(new Insets(100));
      analysisGridPane.setHgap(10);
      analysisGridPane.setVgap(10);
      Label analysisLabel = new Label("Please input the dates");
      analysisGridPane.add(analysisLabel, 1,1);

      DatePicker analysisDatePicker1 = new DatePicker();
      analysisGridPane.add(analysisDatePicker1, 0,0);
      DatePicker analysisDatePicker2 = new DatePicker();
      analysisGridPane.add(analysisDatePicker2, 2,0);
      Button startAnalysis = new Button("Start Analysis");
      startAnalysis.setOnAction(e->{
        Analysis analysisObject = new Analysis(analysisDatePicker1.getValue(), analysisDatePicker2.getValue());
        analysisLabel.setText("Hotel and Roomtype figures:\n" + analysisObject.getHotelAndRoomTypeFigures() + "\n\nBilling figures:\n" + analysisObject.getBillingFigures());
      });

      analysisGridPane.add(startAnalysis, 1, 2);
      Button backToMenuFromAnalysisButton = new Button("Return to main menu");
      backToMenuFromAnalysisButton.setOnAction(e->{
        mainMenu.setScene(mainMenuScene);
      });
      analysisGridPane.add(backToMenuFromAnalysisButton, 1, 3);

      Scene AnalysisWithALowercaseA = new Scene(analysisGridPane);
      analysisGridPane.setPrefWidth(1200);
      analysisGridPane.setPrefHeight(700);
      buttonList.get(3).setOnAction(e->{
        if(admin == false){
          mainMenu.setScene(adminCheckFailed);
        }else{
        mainMenu.setScene(AnalysisWithALowercaseA);
        }
      });

      //quit button
      buttonList.get(4).setOnAction(e->{
        Platform.exit();
      });
    }

    /**
     * Used to launch the GUI.
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
      }
}