package room;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
/**
 * Room Without Cost system
 * @author Michael Byrnes
 */
public class RoomWithoutCost {
    private String RoomType;
    private int OccupancyMin;
    private int OccupancyMax;
    private boolean isReserved;
    private ArrayList<LocalDate> daysReserved;
    private ArrayList<Integer> reservationIDs;
    private ArrayList<Integer> occupancies;
    protected Hotel hotel;
    private String hotelName;
    private static ArrayList<String> hotels = new ArrayList<String>();
    private static ArrayList<String> roomTypes = new ArrayList<String>();
    //This is protected so it can be inherited by room
    protected ArrayList<LocalDate> checkInForCSV = new ArrayList<LocalDate>();
    protected ArrayList<LocalDate> checkOutForCSV = new ArrayList<LocalDate>();
    protected ArrayList<Integer> reservationIDForCSV = new ArrayList<Integer>();
    protected ArrayList<Integer> occupancyForCSV = new ArrayList<Integer>();


    /**
     * Creates a Room without Cost 
     * @param RoomType Type of Room
     * @param OccupancyMin Minimum occupancy of Room
     * @param OccupancyMax Maximum occupancy of Room
     * @param hotelName Name of the hotel
     */
    public RoomWithoutCost(String RoomType, int OccupancyMin, int OccupancyMax, String hotelName){
        hotel = new Hotel(hotelName);
        this.hotelName = hotelName;
        this.RoomType = RoomType;
        this.OccupancyMin = OccupancyMin;
        this.OccupancyMax = OccupancyMax;
        this.isReserved = false;
        this.daysReserved = new ArrayList<LocalDate>();
        this.reservationIDs = new ArrayList<Integer>();
        this.occupancies = new ArrayList<Integer>();

        if (!hotels.contains(hotelName))
            hotels.add(hotelName);

        if (!roomTypes.contains(RoomType))
            roomTypes.add(RoomType);
    }

    /**
     * Checks if the room is occupied on the date given
     * @param date the date to check
     * @return if the room is occupied on the date
     */
    public boolean isOccupiedOn(LocalDate date){
        if (this.daysReserved.contains(date))
            return true;
        return false;
    }

    /**
     * Getter method for getting all hotels
     * @return Hotels in an ArrayList
     */
    public static ArrayList<String> getAllHotelTypes(){
        return hotels;
    }

    /**
     * Setter method for setting occupancy
     * @param occupancy the occupancy
     */
    public void setOccupancy(int occupancy){
        occupancies.add(occupancy);
    }

    /**
     * Getter method for getting all room types
     * @return Room types in an ArrayList
     */
    public static ArrayList<String> getAllRoomTypes(){
        return roomTypes;
    }

    /**
     * Getter method for getting the reservation number on a certain day
     * @param date the date to check
     * @return the reservation number
     */
    public int getReservationIDOnDay(LocalDate date){
        int Id = 0;
        for(int i=0; i< daysReserved.size(); i++){
            if (date == daysReserved.get(i)){
                Id = reservationIDs.get(i);
            }
        }
        return Id;
    }

    /**
     * Getter method for getting the hotel information
     * @return Hotel information
     */
    public String getHotel(){
        return hotelName;
    }

    /**
     * Setter method for setting the type of Room
     * @param roomType Type of Room
     */
    public void setRoomType(String roomType){
        this.RoomType = roomType;
    }

    /**
     * Getter method for getting the type of Room
     * @return Type of Room
     */
    public String getRoomType(){
        return RoomType;
    }

    /**
     * Setter method for setting minimum occupancy of Room
     * @param OccupancyMin Minimum occupancy of Room
     */
    public void setOccupancyMin(int OccupancyMin){
        this.OccupancyMin = OccupancyMin;
    }

    /**
     * Getter method for getting the minimum occupancy of Room
     * @return Minimum occupancy of Room
     */
    public int getOccupancyMin(){
        return OccupancyMin;
    }

    /**
     * Setter method for setting maximum occupancy of Room
     * @param OccupancyMax Maximum occupancy of Room
     */
    public void setOccupancyMax(int OccupancyMax){
        this.OccupancyMax = OccupancyMax;
    }

    /**
     * Getter method for getting the maximum occupancy of Room
     * @return Maximum occupancy of Room
     */
    public int getOccupancyMax(){
        return OccupancyMax;
    }

    /**
     * Getter method for checking if a room is reserved
     * @return if room is reserved
     */
    protected boolean getIsReserved(){
        return isReserved;
    }

    /**
     * Method for reserving a room
     * @param checkin Check-in date
     * @param checkout Check-out date
     * @param reservationNumber The reservation number
     */
    public void reserve(LocalDate checkin, LocalDate checkout, int reservationNumber, int occupancy){
        LocalDate checkincopy=checkin;
        Integer reservationNo = Integer.valueOf(reservationNumber);
        while(checkincopy.isBefore(checkout) || checkincopy.isEqual(checkout)){
            this.daysReserved.add(checkincopy);
            this.reservationIDs.add(reservationNo);
            checkincopy=checkincopy.plusDays(1);
        }
        this.checkInForCSV.add(checkin);
        this.checkOutForCSV.add(checkout);
        this.reservationIDForCSV.add(reservationNumber);
        this.occupancyForCSV.add(occupancy);
    }

    /**
     * Method for cancelling a reservation
     * @param reservationNumber The reservation number
     */
    public void unreserve(int reservationNumber){
        while(reservationIDs.contains(reservationNumber)){
            for(int i = 0; i < reservationIDs.size(); i++){
                if(reservationNumber == reservationIDs.get(i)){
                    reservationIDs.remove(i);
                    daysReserved.remove(i);
                }
            }
        }
    }

    /**
     * Getter method for the ArrayList of days reserved
     * @return ArrayList of days reserved
     */
    public ArrayList<LocalDate> getDates(){
        return daysReserved;
    }

    /**
     * Getter method for the ArrayList of reservation IDs
     * @return ArrayList of reservation IDs
     */
    public ArrayList<Integer> getIDs(){
        return reservationIDs;
    }

    /**
     * Getter method for the ArrayList of reservation IDs
     * @return ArrayList of reservation IDs
     */
    public ArrayList<Integer> getOccupancies(){
        return occupancies;
    }

    /**
     * Getter method for getting the check-in date
     * @return the check-in date
     */
    public LocalDate getCheckin(){
        return daysReserved.get(0);
    }

    /**
     * Getter method for getting the check-out date
     * @return the check-out date
     */
    public LocalDate getCheckout(){
        return daysReserved.get(daysReserved.size()- 1);
    }

    /**
     * Getter method for getting the number of nights stayed
     * @param reservationNumber The reservation number
     * @return nothing
     */
    public int getNightsStayed(int reservationNumber) {
        return Collections.frequency(this.reservationIDs, reservationNumber);
    }
}