package room;

import java.time.temporal.ChronoUnit;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Room system
 * @author Michael Byrnes
 */
public class Room extends RoomWithoutCost{
    private double[] rates = new double[7];
    //{"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"}
    private int roomNumber;
    private static int roomCounter;

    /**
     * Creates a new Room
     * @param roomType Type of Room
     * @param occupancyMin Minimum occupancy of Room
     * @param occupancyMax Maximum occupancy of Room
     * @param rates Array of Room rates
     * @param hotelName Name of the hotel
     */
    public Room(String roomType, int occupancyMin, int occupancyMax, double[] rates, String hotelName) {
        super(roomType, occupancyMin, occupancyMax, hotelName);
        this.rates = rates;
        this.roomNumber = Room.roomCounter++; 
        hotel.setHotelName(hotelName);
    }

    

    /**
     * Getter method for getting the number of Room
     * @return The number of Room
     */
    public int getRoomNumber(){
        return roomNumber;
    }

    /**
     * Getter method for getting the rates of a room
     * @return rates of Room
     */
    public double getRate(int ReservNo) {
        double rate = 0;
        LocalDate firstMon = LocalDate.of(2021, 11, 15);
        long days;
        for(int i = 0; i < getDates().size(); i++){
            if(ReservNo == getIDs().get(i)){
                LocalDate end = getDates().get(i);
                days = ChronoUnit.DAYS.between(firstMon, end);
                int index = ((int)days % 7);
                if(index<0)
                    index+=7;
                rate+=rates[index];
            }
        }
        return rate;
    }

    /**
     * Checks if the room is available
     * @param room The room
     * @return if the room is available
     */
    public boolean canFill(RoomWithoutCost room){
        boolean sameType = false;
        if(room.getHotel().equals(this.getHotel()) && (this.getRoomType().contains(room.getRoomType())) && (this.getOccupancyMin() <= room.getOccupancyMax()) && (room.getOccupancyMax() <= this.getOccupancyMax())){ //our room type is "Deluxe Double", the target is "Double". The room without cost stores the actual occupancy as the max value.
            sameType = true;
        }

        ArrayList<LocalDate> targetDates = room.getDates();      
        Boolean allDatesMatch = true;
        for(int i=0; sameType == true && allDatesMatch==true && i<targetDates.size(); i++){
            if(this.getDates().contains(targetDates.get(i)))
                allDatesMatch = false; 
        } 
        return sameType && allDatesMatch;
    }

    /**
     * Writes variables to csv
     * @return the csv variables
     */
    public String toCSV() {
        //String RoomType, int OccupancyMin, int OccupancyMax, String hotelName, int stars
        String result="";
        //Standard information
        result +=  this.getRoomType() + "," + this.getOccupancyMin() + "," + this.getOccupancyMax() + "," + this.hotel.getHotelName() + ",";
        result+=this.roomNumber + ",";
        //rates
        result+=this.rates[0] + "/" + this.rates[1] + "/" + this.rates[2] + "/" + this.rates[3] + "/" + this.rates[4] + "/" + this.rates[5] + "/" + this.rates[6] + ",";
        for(int i=0; i<this.reservationIDForCSV.size(); i++){
            result+=String.valueOf(this.reservationIDForCSV.get(i)) + ":" + this.checkInForCSV.get(i).toString() + "-" + this.checkOutForCSV.get(i).toString() + ":" + this.occupancyForCSV.get(i);
            if(i+1<this.reservationIDForCSV.size())
                result+="/";
        }
        return result;
    }
}