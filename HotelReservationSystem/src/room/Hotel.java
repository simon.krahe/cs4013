package room;

/**
 * Hotel system
 * @author Michael Byrnes
 */
public class Hotel{
    private String hotelName;

    /**
     * Creates a Hotel
     * @param hotelName Name of the hotel
     */
    public Hotel(String hotelName){
        this.hotelName = hotelName;
    }

    /**
     * Getter method for getting the name of the hotel
     * @return Name of the hotel
     */
    public String getHotelName(){
        return hotelName;
    }

    /**
     * Setter method for setting the name of the hotel
     * @param hotelName
     */
    public void setHotelName(String hotelName){
        this.hotelName = hotelName;
    }
}