# Testing

Due to our feature branch workflow, as soon as we had our skeleton system together, we usually had working code on `main`. This means that when adding a new feature, it could be tested right away if it works and fixed accordingly if needed.

In this folder are CSV output files for our room and reservation data from different stages in development (which is why their style differs). We had them saved as `.txt` but switched to `.csv` at some point.

Evidence that these are actual files created as part of the project can be retrieved from the git history.
