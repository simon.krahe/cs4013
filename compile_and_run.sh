#!/bin/bash

#compile
javafx=~/javafx-sdk-11.0.2/lib

javac \
	-target 11 \
	-source 11 \
	-d classfiles \
	--module-path "$javafx" \
	--add-modules=javafx.controls \
	HotelReservationSystem/src/*/*.java

#run
cp HotelReservationSystem/l4Hotels.csv classfiles
cd classfiles
java \
	--module-path "$javafx" \
	--add-modules "javafx.controls" startup/startup
