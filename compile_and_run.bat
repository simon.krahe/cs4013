REM compile
javac -target 11 -source 11 -d classfiles --module-path "YOU_JAVAFX_PATH_HERE" --add-modules=javafx.controls HotelReservationSystem/src/analysis/Analysis.java HotelReservationSystem/src/billing/Billing.java HotelReservationSystem/src/csvcreation/ReadCSVFile.java HotelReservationSystem/src/reservation/Reservation.java HotelReservationSystem/src/reservation/ReservationException.java HotelReservationSystem/src/room/Hotel.java HotelReservationSystem/src/room/Room.java HotelReservationSystem/src/room/RoomWithoutCost.java HotelReservationSystem/src/startup/startup.java HotelReservationSystem/src/time/Time.java HotelReservationSystem/src/UI/UI.java HotelReservationSystem/src/UI/GUI.java HotelReservationSystem/src/printToCSV/PrintToCSV.java

REM run
copy HotelReservationSystem/l4Hotels.csv classfiles
cd classfiles
java --module-path "YOU_JAVAFX_PATH_HERE" --add-modules "javafx.controls" startup/startup
