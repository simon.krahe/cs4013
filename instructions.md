# Instructions
In order to compile and run the project, execute the `compile_and_run.sh` or `compile_and_run.bat` file for Linux or Windows respectively. The script assumes JavaFX 11.0.2 to be installed in either `~/javafx-sdk-11.0.2` for Linux, which can be changed through the variable at the start. For Windows, manually set the path in the two lines where it is needed. To use Java 17, change the JavaFX path to a JavaFX 17 installation and the `target` and `source` flags on javac accordingly. Alternatively, run the commands directly in the command line.

Link to repository: https://gitlab.com/simon.krahe/cs4013
