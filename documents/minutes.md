# First meeting, 21.10.2021, 10:00-11:30

Attending: All

- We will try to go for an A grade. We will implement everything but the GUI in the spec first and then see if we still have time to do that as well
- We will use git (gitlab, https://gitlab.com/simon.krahe/cs4013) with a feature branch workflow
- We will use SonarQube to facilitate code checks
- We will communicate via discord
- We will meet after lectures and during the lab sessions
- We did the noun identification technique based on the given project spec and did the CRC cards as a group
- We are meeting up next on Wednesday. The tasks assigned until then are as follows: Mahir does the first version of the UML diagram based on our CRC cards, Michael works the Room class, Matthew works on the (text-based) UI, Simon works on the Reservation class and researches how gitlab can be used from the command line when signing in with github accounts


# Second meeting, 28.10.2021, 11:00-11:50

Attending: All

- Next meeting is on monday 14:00 in CS building
- Until then: Matthew continues UI, Mahir continues UML, Michael continues Room, Matthew and Simon do Billing, Mahir does Time, Michael does Hotel

# Third meeting, 01.11.2021, 14:00-16:00

Attending: All

-Conceptualized together and continued working on our assignments

# Fourth meeting, 08.11.2021, 14:00-16:00

Attending: All

- Made individual contributions fit together, first version of system is partially running
- Left to do: Refactoring, GUI, Analysis, CSV handling
- Everyone: Add javadoc
- Michael: Write room data to file, Simon: Write reservation data to file, Mahir: Create room and hotel data from file, Matthew: General refactoring, improving how the desired occupancy is passed from UI to Reservation

# Fifth meeting, 15.11.2021

- Merged code together and made it run
- Michael: Implement room number and storing occupancy + print to CSV
- Divided work for next steps: UI, analysis, UML
- Matthew and Simon will work on analysis and UI, the others will help when/if they are done

# Sixth meeting, 18.11.2021

- Everyone continued working on their tasks
- The analysis asks for billing data within a timeframe. We have decided that this means all reservations that are at least partly within that timeframe.
- In creating a RoomWithoutCost object from the UI and handing that down, we are violating the MVC design principle. We have been advised not to refactor.
- A RoomWithoutCost object is more different from a potential RoomRequest object than anticipated. This means that we will have to do checking (e.g. of the potential occupancy of a room) on the UI instead of on the Room or Reservation object when trying to create a reservation. In a future design, passing down all the information on a room request as method arguments or another type of object would be better. We will continue with the current state and see later if we should refactor and still have the time.
- Tasks stay the same

# Seventh meeting, 22.11.2021

- Matthew worked on GUI, Mahir provided sketches
- Michael worked on some remaining problems in the method of Room outputting to CSV, as well as some issues in business logic
- Simon assisted others
- After consulting with Michael (the lecturer), we will not refactor our architecture. We need to be aware of the issues and understand how we would do it differently next time. We will meet again on Wednesday to put all of the code together in the final version and on Friday to do the remaining documentation and quiz each other on the codebase.
- Until then, Mahir will work on an issue with cancelling a reservation, Michael will refactor some more around room and Matthew and Simon will have the GUI in a running state.

# Eight meeting, 24.11.2021

- Matthew refactored on GUI and extended functionality
- Mahir fixed and tested cancelReservation
- Simon worked fixing on issues spread in the core business logic
- Everyone will familiarise themselves with the code. Tomorrow we will patch the final revision together.

# Ninth meeting, 25.11.2021

- Introduced each other to code by others.
- Various small remaining tasks found and assigned for next meeting.  Mahir does documentation on the contributions. Simon does instructions for compiling and running from the command line, adds comments to analysis and collects evidence of testing. Matthew and Simon will refactor UI - we were told that having a "command line interface" would take priority over a GUI, so we will adapt it to have the same feature set that the GUI currently has. Matthew will introduce user management (on a very small scale) and do very small GUI refactoring. Michael will change the core logic so that occupancy is passed to room/roomWithoutCost, stored there and output to CSV, as well as go over the UML.
- The next meeting should be short and is only meant to go over changes in code and everything we will finally hand in.

# Tenth meeting, 29.11.2021
- Worked on occupancy for room
- Minor remaining fixes

# Eleventh meeting, 30.11.2021
- Agreed on contributions in new template
- Final check and submission of project
